import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mytodo',
  templateUrl: './mytodo.component.html',
  styleUrls: [
    './mytodo.component.css',
  ]
})
export class MytodoComponent implements OnInit {

  title: string = "My list todo";

  constructor() { }

  ngOnInit() {
  }

}
